#!/bin/sh

modprobe i2c-bcm2835
modprobe i2c-dev
modprobe rtc-ds1307

echo ds1307 0x68 > /sys/class/i2c-adapter/i2c-1/new_device
hwclock -s
