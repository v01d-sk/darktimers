#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <netinet/in.h>


int machine_id(char* const buffer, const size_t length)
{
    if (length < 12)
    {
        return -1;
    }

    // Source:
    // https://stackoverflow.com/questions/1779715/how-to-get-mac-address-of-your-machine-using-a-c-program

    int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
    if (sock == -1)
    {
        return -2;
    }

    char buf[1024];
    struct ifconf ifc;
    ifc.ifc_len = sizeof(buf);
    ifc.ifc_buf = buf;
    if (ioctl(sock, SIOCGIFCONF, &ifc) == -1)
    {
        return -3;
    }

    struct ifreq* it = ifc.ifc_req;
    const struct ifreq* const end = it + (ifc.ifc_len / sizeof(struct ifreq));
    int success = 0;
    for (; it != end; ++it)
    {
        struct ifreq ifr;
        strcpy(ifr.ifr_name, it->ifr_name);
        if(ioctl(sock, SIOCGIFFLAGS, &ifr) != 0)
        {
            continue;
        }
        if(ifr.ifr_flags & IFF_LOOPBACK)
        {
            continue;
        }

        if (ioctl(sock, SIOCGIFHWADDR, &ifr) == 0)
        {
            uint8_t* const mac = ifr.ifr_hwaddr.sa_data;
            return snprintf(buffer, length, "%.2X%.2X%.2X%.2X%.2X%.2X",
                            mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
        }
    }

    return -4;
}
