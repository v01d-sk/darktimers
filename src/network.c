#include "network.h"
#include "loop.h"

#include <arpa/inet.h>
#include <sys/socket.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <stdio.h>
#include <stdlib.h>

#define WPA_SUPPLICANT_CONF "/etc/wpa_supplicant/wpa_supplicant.conf"

#define CHECK_CONNECTION_TIMEOUT 1000
#define WAIT_FOR_CONNECTION_TIMEOUT 60000

static NETWORK_CALLBACK network_callback;
static enum NetworkState current_state = NETWORK_UNKNOWN_STATE;
static int check_conn_timer_id = INVALID_TIMER_ID;
static int wait_conn_timer_id = INVALID_TIMER_ID;
static const char * network_wifi_essid = NULL;
static const char * network_wifi_psk = NULL;
static const char * network_wifi_start_cmd = NULL;
static const char * network_wifi_stop_cmd = NULL;

static bool is_connected()
{
    struct ifaddrs *ifap, *ifa;
    struct sockaddr_in * sa;
    char *addr;

    bool found = false;

    getifaddrs (&ifap);
    for (ifa = ifap; ifa; ifa = ifa->ifa_next) {
        if (ifa->ifa_addr && ifa->ifa_addr->sa_family == AF_INET && !(ifa->ifa_flags & IFF_LOOPBACK) && (ifa->ifa_flags & IFF_UP)) {
            sa = (struct sockaddr_in *) ifa->ifa_addr;
            addr = inet_ntoa(sa->sin_addr);
            printf("Found interface that is up: %s\tAddress: %s\n", ifa->ifa_name, addr);

            found = true;
            break;
        }
    }

    freeifaddrs(ifap);
    return found;
}

static void check_connection_callback(int timer_id, void * data)
{
    loop_clear_timer(timer_id);

    if (timer_id == wait_conn_timer_id)
        loop_stop_timer(timer_id);

    bool connected = is_connected();

    enum NetworkState new_state = NETWORK_UNKNOWN_STATE;
    if (connected)
    {
        new_state = NETWORK_CONNECTED;
        loop_stop_timer(wait_conn_timer_id);
    }
    else
    {
        // If we are connecting and we are just regularly checking for connection, stay connecting.
        // Otherwise disconnected (the waiting for connecting timer expired).
        if (current_state == NETWORK_CONNECTING && timer_id == check_conn_timer_id)
            new_state = NETWORK_CONNECTING;
        else
            new_state = NETWORK_DISCONNECTED;
    }

    // Timer expired, not connected, stop wifi.
    if (current_state == NETWORK_CONNECTING && timer_id == wait_conn_timer_id && !connected)
        network_stop_wifi();

    if (current_state != new_state)
    {
        current_state = new_state;
        if (network_callback)
            (*network_callback)(current_state);
    }
}

void network_start_wifi(void)
{
    if (current_state != NETWORK_DISCONNECTED)
        return;

    network_config_wifi();

    current_state = NETWORK_CONNECTING;
    if (network_callback)
        (*network_callback)(NETWORK_CONNECTING);

    loop_start_timer(wait_conn_timer_id, WAIT_FOR_CONNECTION_TIMEOUT);
}

void network_stop_wifi(void)
{
    printf("Starting network command '%s'.\n", network_wifi_stop_cmd);
    system(network_wifi_stop_cmd);
}

bool network_config_wifi(void)
{
    FILE *wpa_config = fopen(WPA_SUPPLICANT_CONF,"w");
    if (wpa_config == NULL)
    {
        fprintf(stderr,"Can not open wpa_supplicant config file: %s\n", WPA_SUPPLICANT_CONF);
        return false;
    }
    fprintf(wpa_config,"network={\n\tssid=\"%s\"\n\tpsk=\"%s\"\n}\n", network_wifi_essid, network_wifi_psk);
    fclose(wpa_config);
    printf("Starting network command '%s'.\n", network_wifi_start_cmd);
    system(network_wifi_start_cmd);
    return true;
}

bool network_init(NETWORK_CALLBACK callback, const char * essid, const char * psk, const char * start_cmd, const char * stop_cmd)
{
    check_conn_timer_id = loop_add_timer(check_connection_callback, NULL);
    if (check_conn_timer_id == INVALID_TIMER_ID)
        return false;
    wait_conn_timer_id = loop_add_timer(check_connection_callback, NULL);
    if (wait_conn_timer_id == INVALID_TIMER_ID)
    {
        loop_remove_timer(check_conn_timer_id);
        return false;
    }

    loop_start_timer(check_conn_timer_id, CHECK_CONNECTION_TIMEOUT);

    network_callback = callback;
    network_wifi_essid = essid;
    network_wifi_psk = psk;
    network_wifi_start_cmd = start_cmd;
    network_wifi_stop_cmd = stop_cmd;

    return true;
}

void network_destroy(void)
{
    if (check_conn_timer_id != INVALID_TIMER_ID)
        loop_remove_timer(check_conn_timer_id);
    if (wait_conn_timer_id != INVALID_TIMER_ID)
        loop_remove_timer(wait_conn_timer_id);
}

