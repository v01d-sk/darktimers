#ifndef RFID_H_W295CXNK
#define RFID_H_W295CXNK

#include <sys/time.h>
#include <stdbool.h>
#include <stdio.h>

/** RFID subsystem data.
 */
struct rfid_data_t
{
    /// Filename of the RFID input ("keyboard") file.
    const char* path_rfid;
    /// File descriptor of the RFID file.
    int file_rfid;

    /// ID read from the reader.
    char id[64];
    /// Length of used data in id.
    int id_length;
    /** Are we currently reading an ID?
     *
     * Set to true when we get a key event; following key events add to
     * rfid_data_t.id. Set to false by rfid_timer_callback if READ_TIMEOUT_SECONDS
     * has elapsed since the initial read event (the one that set reading to true).
     */
    bool reading;
    /// Timestamp of the initial  key event that set reading to true.
    struct timeval time_read_start;
    /// Filename of the RFID output (csv) file.
    const char* path_output;

    char prev_id[64];
    double prev_time;
};

/** Initialize the RFID subsystem, open files, register poll loop callback.
 *
 * Returns true on success, false on failure.
 */
bool rfid_init(struct rfid_data_t* const rfid, const char* const path_rfid,
               const char* const path_output);
void rfid_free(struct rfid_data_t* const rfid);

#endif /* end of include guard: RFID_H_W295CXNK */
