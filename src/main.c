#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <linux/limits.h>
#include <limits.h>

#include "config.h"
#include "loop.h"
#include "machineid.h"
#include "rfid.h"
#include "gpio.h"
#include "button.h"
#include "network.h"
#include "upload.h"
#include "led.h"
#include "buzzer.h"

#define CONNECTING_BLINK_TIME 500
#define WIFI_BUTTON_KEEP_CONNECTED_TIME 5

// QUICK HACK: these globals should not be globals.
static char out_path[PATH_MAX];
static struct config_t config;

static void heartbeat_callback(int timer_id, void * data)
{
    static bool state = 0;

    GPIO * gpio = (GPIO *) data;
    gpio_write(gpio, state ? GPIO_HIGH : GPIO_LOW);

    state = !state;
    loop_clear_timer(timer_id);
}

static void wifi_button_callback(BUTTON * button, enum ButtonEvent button_event, void * data)
{
    printf("Button changed value to %s!\n", button_event == EVENT_BUTTON_DOWN ? "pressed" : "depressed");

    static time_t last_time = 0;

    // TODO Don't allow multiple times while it's connecting!
    if (button_event == EVENT_BUTTON_UP)
    {
        time_t this_time = time(NULL);
        const bool keep_connected = (difftime(this_time, last_time) >= WIFI_BUTTON_KEEP_CONNECTED_TIME);
        upload_connect(keep_connected);
    }
    else if (button_event == EVENT_BUTTON_DOWN)
    {
        last_time = time(NULL);
    }
}

static void shutdown_callback(BUTTON * button, enum ButtonEvent button_event, void * data)
{
    if (button_event == EVENT_BUTTON_DOWN)
        kill(getpid(), SIGINT);
}

static void generate_out_path( char* const path, const size_t length, const struct config_t* cfg )
{
    int offset = strlen(cfg->rfid_output_dir);
    if (offset > 0)
    {
        strncpy(path, cfg->rfid_output_dir, length);
        strncat(path, "/", length);
        offset++;
    }

    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char hostname[HOST_NAME_MAX + 1] = {0};
    gethostname(hostname, sizeof(hostname));
    hostname[HOST_NAME_MAX] = '\0'; // Not guaranteed to be null-terminated on truncation.
    strncat(path, hostname, length);

    offset += strlen(hostname);
    strftime(path + offset, length - 1 - offset, "_%d_%m_%Y_%H:%M.csv", t);
}

static void network_callback(enum NetworkState state)
{
    const char * str_state = NULL;
    switch (state)
    {
        case NETWORK_CONNECTED:
            str_state = "connected";
            led_set_permanent(true);
            upload_start();
            // Need to generate new output path so RFID code does not start
            // overwriting the original that was uploaded
            generate_out_path(out_path, sizeof(out_path), &config);
            fprintf(stderr, "Updated out_path: '%s'\n", out_path);
            break;
        case NETWORK_DISCONNECTED:
            str_state = "disconnected";
            led_set_permanent(false);
            break;
        case NETWORK_CONNECTING:
            str_state = "connecting";
            led_set_blink(CONNECTING_BLINK_TIME);
            break;
    }
    printf("Network state changed: %s.\n", str_state);
}

static struct sigaction sigint_old_action;
static struct sigaction sigterm_old_action;
static void signal_handler(int sig_no)
{
    puts("Signal received.\n");
    sigaction(SIGINT, &sigint_old_action, NULL);
    sigaction(SIGTERM, &sigterm_old_action, NULL);
    loop_stop();
}

int main(int argc, char** argv)
{
    int ret = 0;

    struct sigaction action;
    memset(&action, 0, sizeof(action));
    action.sa_handler = &signal_handler;
    sigaction(SIGINT, &action, &sigint_old_action);
    sigaction(SIGTERM, &action, &sigterm_old_action);

    /* default path in home, overridable by first CLI arg */
    const char* config_path = "darktimersrc";
    if (argc >= 2)
    {
        config_path=argv[1];
    }

    if (!config_init(&config, config_path))
    {
        return 1;
    }

    if (config.gpio_wifi_button <= 0 || config.gpio_heartbeat <= 0 || config.gpio_led <= 0 || config.gpio_shutdown <= 0)
    {
        fprintf(stderr, "Please make sure gpio_button, gpio_heartbeat and gpio_led are set in the config.\n");
        return 2;
    }

    if (!network_init(network_callback, config.wpa_essid, config.wpa_psk, config.wpa_start_cmd, config.wpa_stop_cmd))
    {
        fprintf(stderr, "Failed to initialize network.\n");
        ret = 3;
        goto error_network;
    }

    if (!led_init(config.gpio_led))
    {
        fprintf(stderr, "Failed to initialize LED.\n");
        ret = 4;
        goto error_led;
    }

    led_set_permanent(false);

    if (config.buzzer_on)
    {
        if (!buzzer_init(config.gpio_buzzer, config.starttime_interval, config.buzzer_use_led))
        {
            fprintf(stderr, "Failed to initialize buzzer.\n");
            ret = 5;
            goto error_buzzer;
        }
    }

    GPIO * heartbeat = gpio_open(config.gpio_heartbeat, GPIO_OUT, GPIO_EDGE_UNKNOWN);
    if (!heartbeat)
    {
        fprintf(stderr, "Failed to initialize heartbeat.\n");
        ret = 6;
        goto error_heartbeat;
    }

    BUTTON * wifi_button = button_create(config.gpio_wifi_button, GPIO_LOW, GPIO_HIGH, wifi_button_callback, NULL);
    if (!wifi_button)
    {
        fprintf(stderr, "Failed to initialize button.\n");
        ret = 7;
        goto error_wifi_button;
    }

    BUTTON * shutdown_button = button_create(config.gpio_shutdown, GPIO_HIGH, GPIO_LOW, shutdown_callback, NULL);
    if (!shutdown_button)
    {
        fprintf(stderr, "Failed to initialize button.\n");
        ret = 8;
        goto error_shutdown_button;
    }

    generate_out_path(out_path, sizeof(out_path), &config);
    fprintf(stderr, "out_path: '%s'\n", out_path);

    struct rfid_data_t rfid;
    if(!rfid_init(&rfid, config.path_rfid, out_path))
    {
        fprintf(stderr, "Failed to initialize RFID.\n");
        ret = 9;
        goto error_rfid;
    }

    int heartbeat_timer = loop_add_timer(&heartbeat_callback, heartbeat);
    loop_start_timer(heartbeat_timer, 500);

    if (!upload_init(config.sftp_server, config.sftp_username, config.sftp_password, config.rfid_output_dir, config.sftp_server_path, config.sftp_server_ready_file))
    {
        fprintf(stderr, "Failed to initialize upload.\n");
        ret = 10;
        goto error_upload;
    }

    loop_run();

    finish:
        loop_remove_timer(heartbeat_timer);
    error_heartbeat_timer:
        upload_destroy();
    error_upload:
        rfid_free(&rfid);
    error_rfid:
        button_destroy(wifi_button);
    error_shutdown_button:
        button_destroy(shutdown_button);
    error_wifi_button:
        gpio_close(heartbeat);
    error_heartbeat:
        if (config.buzzer_on)
            buzzer_destroy();
    error_buzzer:
        led_destroy();
    error_led:
        network_destroy();
    error_network:
        return ret;
}
