#ifndef CONFIG_H_KO8K9RVM
#define CONFIG_H_KO8K9RVM

#include <stdbool.h>

#define CONFIG_MAX_STR_SIZE 512

struct config_t
{
    char path_rfid[CONFIG_MAX_STR_SIZE];
    char rfid_output_dir[CONFIG_MAX_STR_SIZE];
    int gpio_wifi_button;
    int gpio_heartbeat;
    int gpio_led;
    int gpio_buzzer;
    int gpio_shutdown;
    bool buzzer_on;
    bool buzzer_use_led;
    int  starttime_interval;
    char sftp_server[CONFIG_MAX_STR_SIZE];
    char sftp_username[CONFIG_MAX_STR_SIZE];
    char sftp_password[CONFIG_MAX_STR_SIZE];
    char sftp_client_path[CONFIG_MAX_STR_SIZE];
    char sftp_server_path[CONFIG_MAX_STR_SIZE];
    char sftp_server_ready_file[CONFIG_MAX_STR_SIZE];
    char wpa_essid[CONFIG_MAX_STR_SIZE];
    char wpa_psk[CONFIG_MAX_STR_SIZE];
    char wpa_start_cmd[CONFIG_MAX_STR_SIZE];
    char wpa_stop_cmd[CONFIG_MAX_STR_SIZE];
};

bool config_init( struct config_t* const cfg, const char* const config_path );

#endif /* end of include guard: CONFIG_H_KO8K9RVM */
