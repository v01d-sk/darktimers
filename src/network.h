#pragma once

#include <stdbool.h>

enum NetworkState
{
    NETWORK_UNKNOWN_STATE,
    NETWORK_CONNECTED,
    NETWORK_CONNECTING,
    NETWORK_DISCONNECTED
};

typedef void (*NETWORK_CALLBACK)(enum NetworkState state);

void network_start_wifi(void);
void network_stop_wifi(void);
bool network_config_wifi(void);

bool network_init(NETWORK_CALLBACK callback, const char * essid, const char * psk, const char * start_cmd, const char * stop_cmd);
void network_destroy(void);
