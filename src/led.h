#pragma once
#include <stdbool.h>

bool led_init(int gpio_num);
void led_destroy(void);
void led_set_permanent(bool on);
void led_set_blink(int ms);
