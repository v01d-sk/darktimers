#pragma once

#include <stdbool.h>

bool upload_init(const char * server_ip, const char * username, const char * password, const char * client_path, const char * server_path, const char * server_ready_file);
bool upload_connect(bool keep_connected);
bool upload_start(void);
void upload_destroy(void);
