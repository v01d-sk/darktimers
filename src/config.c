#include "config.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static bool read_string(const char * key, const char * value, const char * expected_key, char * out_string)
{
    // - 1 for \0.
    if (0 == strncmp(key, expected_key, CONFIG_MAX_STR_SIZE - 1))
    {
        strncpy(out_string, value, CONFIG_MAX_STR_SIZE - 1);
        printf("%s: %s\n", key, out_string);
		return true;
    }
	return false;
}

static bool read_int(const char * key, const char * value, const char * expected_key, int * out_int)
{
    if (0 == strncmp(key, expected_key, CONFIG_MAX_STR_SIZE - 1))
	{
		*out_int = atoi(value);
        printf("%s: %d\n", key, *out_int);
        return true;
	}
	return false;
}

static bool read_bool(const char * key, const char * value, const char * expected_key, bool * out_bool)
{
    if (0 == strncmp(key, expected_key, CONFIG_MAX_STR_SIZE - 1))
	{
		*out_bool = (bool) atoi(value);
        printf("%s: %s\n", key, *out_bool ? "true" : "false");
        return true;
	}
    return false;
}

bool config_init( struct config_t* const cfg, const char* const config_path )
{
    FILE* file = fopen(config_path, "rb");
    if (0 == file)
    {
        fprintf(stderr, "Failed to open '%s': %s\n", config_path, strerror(errno));
        return false;
    }
    memset(cfg, 0, sizeof(*cfg));

    while(true)
    {
        char file_line[CONFIG_MAX_STR_SIZE * 2 + 2] = {0};
        if (fgets(file_line, sizeof(file_line), file) == NULL)
            break; /* End of file */

        if (file_line[strlen(file_line) - 1] == '\n')
            file_line[strlen(file_line) - 1] = '\0';

        char * delim_pos = strchr(file_line, ' ');
        if (!delim_pos)
            continue; /* not a config line */

        *delim_pos = '\0';
        const char * key = file_line;
        const char * value = delim_pos + 1;

		if (read_string(key, value, "rfid_input_path", cfg->path_rfid))
			continue;
		if (read_string(key, value, "rfid_output_dir", cfg->rfid_output_dir))
			continue;
		if (read_int(key, value, "gpio_wifi_button", &cfg->gpio_wifi_button))
			continue;
		if (read_int(key, value, "gpio_heartbeat", &cfg->gpio_heartbeat))
			continue;
		if (read_int(key, value, "gpio_led", &cfg->gpio_led))
			continue;
		if (read_int(key, value, "gpio_buzzer", &cfg->gpio_buzzer))
			continue;
		if (read_int(key, value, "gpio_shutdown", &cfg->gpio_shutdown))
			continue;
		if (read_bool(key, value, "buzzer_on", &cfg->buzzer_on))
			continue;
		if (read_bool(key, value, "buzzer_use_led", &cfg->buzzer_use_led))
			continue;
		if (read_int(key, value, "starttime_interval", &cfg->starttime_interval))
			continue;
		if (read_string(key, value, "sftp_server", cfg->sftp_server))
			continue;
		if (read_string(key, value, "sftp_username", cfg->sftp_username))
			continue;
		if (read_string(key, value, "sftp_password", cfg->sftp_password))
			continue;
		if (read_string(key, value, "sftp_server_path", cfg->sftp_server_path))
			continue;
		if (read_string(key, value, "sftp_server_ready_file", cfg->sftp_server_ready_file))
			continue;
		if (read_string(key, value, "wpa_essid", cfg->wpa_essid))
			continue;
		if (read_string(key, value, "wpa_psk", cfg->wpa_psk))
			continue;
		if (read_string(key, value, "wpa_start_cmd", cfg->wpa_start_cmd))
			continue;
		if (read_string(key, value, "wpa_stop_cmd", cfg->wpa_stop_cmd))
			continue;

    }

    if (ferror(file))
    {
        fprintf(stderr, "Error reading '%s': %s", config_path, strerror(errno));
    }
    fclose(file);
    return true;
}


