#include "rfid.h"

#include <linux/input.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "buzzer.h"

#include "loop.h"

static char input_code_to_char( uint16_t code )
{
    switch (code)
    {
        case KEY_0:     return '0';
        case KEY_1:     return '1';
        case KEY_2:     return '2';
        case KEY_3:     return '3';
        case KEY_4:     return '4';
        case KEY_5:     return '5';
        case KEY_6:     return '6';
        case KEY_7:     return '7';
        case KEY_8:     return '8';
        case KEY_9:     return '9';
        case KEY_ENTER: return '\n';
    }
    return 'E';
}

static double timeval_seconds( struct timeval* tv )
{
    return (tv->tv_sec + tv->tv_usec * 0.000001);
}

static int open_rfid_reader( const char* const path )
{
    return open(path, O_RDONLY | O_NONBLOCK);
}

/** Called when there's data to read from the RFID input file.
 */
static void rfid_read_callback(void* data)
{
    struct rfid_data_t* const rfid = data;
    struct input_event ev;

    const int read_bytes = read(rfid->file_rfid, &ev, sizeof(struct input_event));
    if(read_bytes < sizeof(struct input_event))
    {
        // TODO handle this? We get this ad infinitum when we pull the reader out
        // We'd need to remove the file from the poll loop, close it, re-open and
        // re-add.
        //perror("Error reading from file");
        fprintf(stderr, ".");
    }

    if(EV_KEY != ev.type)
    {
        return;
    }

    if (!rfid->reading)
    {
        rfid->time_read_start = ev.time;
        rfid->reading = true;
    }

    /* 0: key release, 1: key press, 2: autorepeat*/
    if((1 == ev.value) && (rfid->id_length < (int)sizeof(rfid->id)))
    {
        const char c = input_code_to_char(ev.code);
        /* Newline: end of a code */
        if ('\n' == c)
        {
            // needed for strcmp
            rfid->id[rfid->id_length+1] = '\0';
            FILE * file_out = fopen(rfid->path_output, "ab");
            if (file_out)
            {
                /* fprintf(file_out, */
                /*         "%.*s,%f\n", */
                /*         rfid->id_length, rfid->id, timeval_seconds(&rfid->time_read_start) ); */
                struct timeval tv;
                gettimeofday(&tv, NULL);
                const double seconds = timeval_seconds(&tv);

                bool ignore = (0 == strcmp(rfid->id, rfid->prev_id));

                if (!ignore)
                {
                    fprintf(file_out, "%.*s,%f\n", rfid->id_length, rfid->id, seconds );
                    fflush(file_out);
                    fsync(fileno(file_out));
                }
                else
                {
                    fprintf(stderr, "Ignoring double-beep!\n");
                }
                fclose(file_out);

                // including the null terminator
                memcpy(rfid->prev_id, rfid->id, rfid->id_length + 1);
                rfid->prev_time = seconds;
            }
            else
            {
                perror("ERROR opening output file");
            }

            rfid->reading   = false;
            rfid->id_length = 0;
            buzzer_start();
        }
        else
        {
            rfid->id[rfid->id_length++] = c;
        }
    }
}

bool rfid_init(struct rfid_data_t* const rfid, const char* const path_rfid,
               const char* const path_output)
{
    fprintf(stderr, "Going to open file %s\n", path_rfid);
    memset(rfid, 0, sizeof(*rfid));
    rfid->path_rfid = path_rfid;
    rfid->file_rfid = open_rfid_reader(path_rfid);
    rfid->path_output = path_output;
    rfid->prev_time = 0.0;
    if(0 > rfid->file_rfid)
    {
        perror("ERROR opening RFID file");
        return false;
    }

    if(!loop_add_fd(rfid->file_rfid, &rfid_read_callback, rfid))
    {
        fprintf(stderr, "Error adding file to poll loop\n");
        return false;
    }
    return true;
}

void rfid_free(struct rfid_data_t* const rfid)
{
    if(0 != close(rfid->file_rfid))
    {
        perror("ERROR closing RFID file\n");
    }

    memset(rfid, 0, sizeof(*rfid));
}
