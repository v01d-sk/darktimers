#include "upload.h"
#include "network.h"
#include "led.h"

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <libsftp.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <dirent.h> 
#include <stdio.h>
#include <linux/limits.h>
#include <unistd.h>

#define UPLOAD_FILES_SUFFIX ".csv"
#define CONNECT_DONE_FILE "/tmp/connectdone"

static pthread_t upload_thread_handle;
static unsigned long sftp_hostaddr;
static const char * sftp_username;
static const char * sftp_password;
static const char * sftp_client_path;
static const char * sftp_server_path;
static const char * sftp_server_ready_file;
static bool upload_keep_connected = false;

void wifi_connect_timer();

static pthread_mutex_t upload_started_mutex;
static volatile bool upload_started = false;

static bool str_ends_with(const char * haystack, const char * needle)
{
    const size_t hs_len = strlen(haystack);
    const size_t nd_len = strlen(needle);

    if (nd_len > hs_len)
        return false;

    return strcmp(haystack + (hs_len - nd_len), needle) == 0;
}

static bool check_ready_file(const char * path)
{
    FILE * f = fopen(path, "r");
    if (!f)
        return false;

    int c = fgetc(f);
    fclose(f);
    return (c == '1');
}

static bool is_uploading()
{
    bool uploading = false;
    pthread_mutex_lock(&upload_started_mutex);
    uploading = upload_started;
    pthread_mutex_unlock(&upload_started_mutex);
    return uploading;
}

static bool set_uploading(bool uploading)
{
    pthread_mutex_lock(&upload_started_mutex);
    upload_started = uploading;
    pthread_mutex_unlock(&upload_started_mutex);
}

static bool has_file_to_upload()
{
    DIR * d;
    struct dirent * dir;
    d = opendir(sftp_client_path);
    if (!d)
        return false;

    bool found = false;

    while ((dir = readdir(d)) != NULL)
    {
        if (!str_ends_with(dir->d_name, UPLOAD_FILES_SUFFIX))
            continue;

        found = true;
        break;
    }

    closedir(d);

    return found;
}

static void * upload_thread(void * data)
{
    puts("Starting upload thread.");

    // Don't start thread if not needed.
    if (!has_file_to_upload())
    {
        puts("Nothing to upload, stopping wifi.");
        goto error2;
    }

    libsftp_session * session = libsftp_connect_password(sftp_hostaddr, sftp_username, sftp_password);
    if (!session)
    {
        puts("Connection to SFTP server failed.");
        goto error2;
    }

    char temp_filename[L_tmpnam];
    tmpnam(temp_filename);
    if (libsftp_get(session, sftp_server_ready_file, temp_filename) != 0)
    {
        printf("Ready file '%s' not found.\n", sftp_server_ready_file);
        goto error;
    }

    bool ready = check_ready_file(temp_filename);
    remove(temp_filename);
    if (!ready)
    {
        puts("Ready file not ready.");
        goto error;
    }

    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char str_date_dir[32] = {0};
    strftime(str_date_dir, sizeof(str_date_dir), "/%Y-%m-%d/", t);

    DIR * d;
    struct dirent * dir;
    d = opendir(sftp_client_path);
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            if (!str_ends_with(dir->d_name, UPLOAD_FILES_SUFFIX))
                continue;

            printf("Uploading %s\n", dir->d_name);

            char client_path[PATH_MAX];
            strncpy(client_path, sftp_client_path, PATH_MAX);
            strncat(client_path, "/", PATH_MAX);
            strncat(client_path, dir->d_name, PATH_MAX);

            char server_path[PATH_MAX];
            strncpy(server_path, sftp_server_path, PATH_MAX);
            strncat(server_path, str_date_dir, PATH_MAX);
            strncat(server_path, dir->d_name, PATH_MAX);

            fprintf(stderr, "Info: Trying to upload file '%s' to '%s'.\n", client_path, server_path);
            int ret = libsftp_put(session, client_path, server_path);
            if (ret < 0)
            {
                fprintf(stderr, "Error: File '%s' failed to upload!\n", client_path);
            }
            else
            {
                remove(client_path);
            }
        }

        closedir(d);
    }

    error:
    puts("Disconnecting from SFTP...");
    libsftp_disconnect(session);


    error2:
    if (!upload_keep_connected)
    {
        puts("Stopping wifi...");

        while(true)
        {
            if (access(CONNECT_DONE_FILE, F_OK) != -1)
                break;

            sleep(1);
        }

        network_stop_wifi();
    }
    else
    {
        puts("Skipping wifi stop.");
    }
    set_uploading(false);

    return NULL;
}

bool upload_init(const char * server_ip, const char * username, const char * password, const char * client_path, const char * server_path, const char * server_ready_file)
{
    sftp_hostaddr = inet_addr(server_ip);
    if (sftp_hostaddr == INADDR_NONE)
    {
        fprintf(stderr, "Invalid IP address.\n");
        return false;
    }
    sftp_username = username;
    sftp_password = password;
    sftp_client_path = client_path;
    sftp_server_path = server_path;
    sftp_server_ready_file = server_ready_file;

    pthread_mutex_init(&upload_started_mutex, NULL);

    return true;
}

void upload_destroy(void)
{
    pthread_mutex_destroy(&upload_started_mutex);
}

bool upload_start(void)
{
    if (is_uploading())
        return false;

    set_uploading(true);
    // Start thread.
    if (!pthread_create(&upload_thread_handle, NULL, upload_thread, NULL))
        return false;
}

bool upload_connect(bool keep_connected)
{
    upload_keep_connected = keep_connected;
    network_start_wifi();
}

