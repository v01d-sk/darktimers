#ifndef GPIO_H_P0FGUIMS
#define GPIO_H_P0FGUIMS

#include <stdbool.h>

enum GPIO_Direction
{
   GPIO_IN,
   GPIO_OUT
};

enum GPIO_Value
{
    GPIO_VALUE_UNKNOWN = -1,
    GPIO_LOW,
    GPIO_HIGH
};

enum GPIO_Edge
{
	GPIO_EDGE_UNKNOWN = -1,
	GPIO_NONE,
	GPIO_RISING,
	GPIO_FALLING,
	GPIO_BOTH
};

typedef struct GPIO GPIO;

GPIO * gpio_open(int num, enum GPIO_Direction dir, enum GPIO_Edge edge);
bool gpio_write(GPIO * gpio, enum GPIO_Value val);
enum GPIO_Value gpio_read(GPIO * gpio);
int gpio_get_fd(GPIO * gpio);
bool gpio_close(GPIO * gpio);

#endif /* end of include guard: GPIO_H_P0FGUIMS */
