#include "gpio.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define SYSFS_GPIO_EXPORT   "/sys/class/gpio/export"
#define SYSFS_GPIO_UNEXPORT "/sys/class/gpio/unexport"
#define SYSFS_GPIO_PATH     "/sys/class/gpio/gpio%d/"
#define SYSFS_GPIO_DIRECTION SYSFS_GPIO_PATH "direction"
#define SYSFS_GPIO_VALUE SYSFS_GPIO_PATH "value"
#define SYSFS_GPIO_EDGE SYSFS_GPIO_PATH "edge"

struct GPIO
{
    int num;
    int fd;
};

static bool write_to_file(const char * file, const char * str)
{
    int fd = -1;
    fd = open(file, O_WRONLY);
    if (fd == -1)
        goto err;

    size_t len = strlen(str);
    ssize_t written = write(fd, str, len);
    close(fd);

    return (len == written);

    err:
        return false;
}

static bool write_to_gpio_file(const char * file, int num, const char * str)
{
    char buf[256];
    sprintf(buf, file, num);
    return write_to_file(buf, str);
}

static const char * edge_to_string(enum GPIO_Edge edge)
{
    switch (edge)
    {
        case GPIO_NONE:
            return "none";
        case GPIO_RISING:
            return "rising";
        case GPIO_FALLING:
            return "falling";
        case GPIO_BOTH:
            return "both";
    }

    return NULL;
}

GPIO * gpio_open(int num, enum GPIO_Direction dir, enum GPIO_Edge edge)
{
    GPIO * gpio = malloc(sizeof(GPIO));
    if (!gpio)
        return NULL;
    gpio->num = num;
    gpio->fd = -1;

    char buf[256];
    sprintf(buf, SYSFS_GPIO_VALUE, num);

    struct stat dummy;
    if (stat(buf, &dummy) != 0)
    {
        char str[16];
        sprintf(str, "%d", num);
        if (!write_to_file(SYSFS_GPIO_EXPORT, str))
        {
            free(gpio);
            return NULL;
        }
    }

    int res = write_to_gpio_file(SYSFS_GPIO_DIRECTION, num, dir == GPIO_IN ? "in" : "out");
    if (!res)
        goto err;

    if (edge != GPIO_EDGE_UNKNOWN)
    {
        res = write_to_gpio_file(SYSFS_GPIO_EDGE, num, edge_to_string(edge));
        if (!res)
            goto err;
    }

    gpio->fd = open(buf, dir == GPIO_IN ? O_RDONLY : O_WRONLY);
    if (gpio->fd == -1)
        goto err;

    return gpio;

    err:
        gpio_close(gpio);

    return NULL;
}

bool gpio_close(GPIO * gpio)
{
    char str[16];
    sprintf(str, "%d", gpio->num);
    write_to_file(SYSFS_GPIO_UNEXPORT, str);

    if (gpio->fd != -1)
        close(gpio->fd);

    free(gpio);
    return true;
}

bool gpio_write(GPIO * gpio, enum GPIO_Value val)
{
    if (gpio->fd == -1)
        return false;

    if (write(gpio->fd, val == GPIO_LOW ? "0" : "1", 1) != 1)
        return false;

    return true;
}

enum GPIO_Value gpio_read(GPIO * gpio)
{
    /* After poll, we need to lseek to the beginning. */
    lseek(gpio->fd, 0, SEEK_SET);

    char c = 0;
    if (read(gpio->fd, &c, 1) != 1)
        return GPIO_VALUE_UNKNOWN;

    if (c == '0')
        return GPIO_LOW;
    if (c == '1')
        return GPIO_HIGH;

    return GPIO_VALUE_UNKNOWN;
}

int gpio_get_fd(GPIO * gpio)
{
    return gpio->fd;
}
