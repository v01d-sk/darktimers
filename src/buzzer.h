#include <stdbool.h>

bool buzzer_init(int gpio_num, int time_interval, bool use_led);
void buzzer_start(void);
void buzzer_destroy(void);

