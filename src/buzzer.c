#include "buzzer.h"
#include "gpio.h"
#include "loop.h"
#include "led.h"
#include <stdlib.h>

#define BUZZER_ON  GPIO_HIGH
#define BUZZER_OFF GPIO_LOW

static GPIO * gpio = NULL;
static int timer_id = INVALID_TIMER_ID;
static int buzz_wait_time = 0;
static int buzz_ring_time = 0;
static bool buzz_use_led = false;
static int notify_time = 10000;
static int beep_interval=1000;
static int beep_count=3;

enum State
{
    STATE_UNUSED,
    STATE_OFF,
    STATE_NOTIFY,
    STATE_NOTIFY_OFF,
    STATE_RING_COUNTER,
    STATE_RING_COUNTER_WAIT,
    STATE_END_RING,
    STATE_END_RING_OFF,
};

static enum State buzz_state = STATE_UNUSED;
static int counter;

static void timer_callback(int timer_id, void * data)
{
    loop_clear_timer(timer_id);
    loop_stop_timer(timer_id);

    switch (buzz_state)
    {
        case STATE_OFF:
            return;
        case STATE_NOTIFY:
            if (!buzz_use_led)
                gpio_write(gpio, BUZZER_ON);
            else
                led_set_permanent(true);
            buzz_state = STATE_NOTIFY_OFF,
            loop_start_timer(timer_id, beep_interval / 2);
            break;

        case STATE_NOTIFY_OFF:
            if (!buzz_use_led)
                gpio_write(gpio, BUZZER_OFF);
            else
                led_set_permanent(false);
            buzz_state = STATE_RING_COUNTER;
            counter = beep_count;
            loop_start_timer(timer_id, notify_time - (beep_interval * (beep_count + 1)));
            break;

        case STATE_RING_COUNTER:
            if (!buzz_use_led)
                gpio_write(gpio, BUZZER_ON);
            else
                led_set_permanent(true);
            buzz_state = STATE_RING_COUNTER_WAIT;
            loop_start_timer(timer_id, beep_interval / 2);
            break;

        case STATE_RING_COUNTER_WAIT:
            if (!buzz_use_led)
                gpio_write(gpio, BUZZER_OFF);
            else
                led_set_permanent(false);
            counter --;
            if (counter == 0)
            {
                buzz_state = STATE_END_RING;
            }
            else 
            {
                buzz_state = STATE_RING_COUNTER;
            }
            loop_start_timer(timer_id, beep_interval / 2);
            break;

        case STATE_END_RING:
            if (!buzz_use_led)
                gpio_write(gpio, BUZZER_ON);
            else
                led_set_permanent(true);
            buzz_state = STATE_END_RING_OFF;
            loop_start_timer(timer_id, beep_interval);
            break;

        case STATE_END_RING_OFF:
            if (!buzz_use_led)
                gpio_write(gpio, BUZZER_OFF);
            else
                led_set_permanent(false);
            buzz_state = STATE_OFF;
            break;
    }
}

bool buzzer_init(int gpio_num, int time_interval, bool use_led)
{
    if (!use_led)
    {
        gpio = gpio_open(gpio_num, GPIO_OUT, GPIO_EDGE_UNKNOWN);
        if (!gpio)
            return false;
    }
    if (time_interval > notify_time) //10s
    {
        timer_id = loop_add_timer(&timer_callback, NULL);
        if (timer_id == INVALID_TIMER_ID)
        {
            gpio_close(gpio);
            return false;
        }

        buzz_wait_time = time_interval - notify_time;
        buzz_state = STATE_OFF;
        buzz_use_led = use_led;
    }
    return true;
}

void buzzer_start(void)
{
    if (buzz_state == STATE_UNUSED)
        return;

    if (buzz_state != STATE_OFF)
    {
        loop_stop_timer(timer_id);
    }

    buzz_state = STATE_NOTIFY;
    loop_start_timer(timer_id, buzz_wait_time);
}

void buzzer_destroy(void)
{
    if (gpio)
        gpio_close(gpio);
    if (timer_id != INVALID_TIMER_ID)
        loop_remove_timer(timer_id);
}

