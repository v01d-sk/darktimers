#ifndef LOOP_H_9NIEX2GQ
#define LOOP_H_9NIEX2GQ

#include <stdbool.h>

typedef void (*LOOP_CALLBACK)(void *);
typedef void (*TIMER_CALLBACK)(int, void *);

#define INVALID_TIMER_ID -1

bool loop_add_fd(int fd, LOOP_CALLBACK callback, void * data);
bool loop_add_gpio_fd(int fd, LOOP_CALLBACK callback, void * data);
int loop_add_timer(TIMER_CALLBACK callback, void * data);
bool loop_start_timer(int timer_id, int msec);
bool loop_stop_timer(int timer_id);
void loop_clear_timer(int timer_id);
void loop_remove_timer(int timer_id);
void loop_run(void);
void loop_stop(void);

#endif /* end of include guard: LOOP_H_9NIEX2GQ */
