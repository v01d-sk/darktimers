#pragma once
#include "gpio.h"

typedef struct BUTTON BUTTON;

enum ButtonEvent
{
	EVENT_BUTTON_DOWN,
	EVENT_BUTTON_UP
};

typedef void (*BUTTON_CALLBACK)(BUTTON *, enum ButtonEvent, void *);

BUTTON * button_create(int gpio_num, enum GPIO_Value gpio_value_down, enum GPIO_Value gpio_value_up, BUTTON_CALLBACK callback, void * data);
void button_destroy(BUTTON * button);
