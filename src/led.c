#include "led.h"
#include "loop.h"
#include "gpio.h"
#include <stdlib.h>

#define LED_ON  GPIO_HIGH
#define LED_OFF GPIO_LOW

static int timer_id = INVALID_TIMER_ID;
static GPIO * gpio = NULL;
enum State
{
    STATE_PERM,
    STATE_BLINK
};
static enum State state = STATE_PERM;
static bool led_on = 0;

static void timer_callback(int timer_id, void * data)
{
    loop_clear_timer(timer_id);

    // Protect from corrupting led state if blinking has been just stopped.
    if (state != STATE_BLINK)
        return;

    led_on = !led_on;
    gpio_write(gpio, led_on ? LED_ON : LED_OFF);
}

bool led_init(int gpio_num)
{
    gpio = gpio_open(gpio_num, GPIO_OUT, GPIO_EDGE_UNKNOWN);
    if (!gpio)
        return false;

    timer_id = loop_add_timer(&timer_callback, NULL);
    if (timer_id == INVALID_TIMER_ID)
    {
        gpio_close(gpio);
        return false;
    }

    return true;
}

void led_destroy(void)
{
    if (gpio)
    {
        gpio_write(gpio, LED_ON);
        gpio_close(gpio);
    }
    if (timer_id != INVALID_TIMER_ID)
        loop_remove_timer(timer_id);
}

void led_set_permanent(bool on)
{
    if (state == STATE_BLINK)
        loop_stop_timer(timer_id);

    gpio_write(gpio, on ? LED_ON : LED_OFF);
    led_on = on;
    state = STATE_PERM;
}

void led_set_blink(int ms)
{
    if (state == STATE_BLINK)
        loop_stop_timer(timer_id);

    loop_start_timer(timer_id, ms);
    state = STATE_BLINK;
}
