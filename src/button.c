#include "button.h"
#include "loop.h"
#include "gpio.h"
#include <stdio.h>
#include <stdlib.h>

#define BUTTON_DOWN GPIO_HIGH
#define BUTTON_UP   GPIO_LOW
#define BUTTON_CHECK_TIMEOUT 20

enum ButtonState
{
    STATE_DOWN,
    STATE_MIDDLE_TO_UP,
    STATE_MIDDLE_TO_DOWN, // Not supported yet.
    STATE_UP
};

struct BUTTON
{
    GPIO * gpio;
    enum ButtonState state;
    int timer_id;
    BUTTON_CALLBACK callback;
    void * data;
    enum GPIO_Value gpio_value_down;
    enum GPIO_Value gpio_value_up;
};

static void gpio_callback(void * data)
{
    BUTTON * button = (BUTTON *) data;
    enum GPIO_Value value = gpio_read(button->gpio);

    if (value == button->gpio_value_down)
    {
        switch (button->state)
        {
            case STATE_DOWN:
            case STATE_MIDDLE_TO_DOWN:
                // Nothing to do.
                break;
            case STATE_MIDDLE_TO_UP:
                loop_stop_timer(button->timer_id);
                button->state = STATE_DOWN;
                break;
            case STATE_UP:
                button->state = STATE_MIDDLE_TO_DOWN;
                loop_start_timer(button->timer_id, BUTTON_CHECK_TIMEOUT);
                break;
        }
    }
    else if (value == button->gpio_value_up)
    {
        switch (button->state)
        {
            case STATE_UP:
            case STATE_MIDDLE_TO_UP:
                // Nothing to do.
                break;
            case STATE_MIDDLE_TO_DOWN:
                loop_stop_timer(button->timer_id);
                button->state = STATE_UP;
                break;
            case STATE_DOWN:
                button->state = STATE_MIDDLE_TO_UP;
                loop_start_timer(button->timer_id, BUTTON_CHECK_TIMEOUT);
                break;
        }
    }
}

static void timer_callback(int timer_id, void * data)
{
    BUTTON * button = (BUTTON *) data;
    loop_clear_timer(button->timer_id);
    loop_stop_timer(timer_id);

    if (button->state != STATE_MIDDLE_TO_UP && button->state != STATE_MIDDLE_TO_DOWN)
        return;

    enum GPIO_Value value = gpio_read(button->gpio);

    if (button->state == STATE_MIDDLE_TO_UP)
    {
        if (value == button->gpio_value_up)
        {
            button->state = STATE_UP;
            (*button->callback)(button, EVENT_BUTTON_UP, button->data);
        }
        else
        {
            button->state = STATE_DOWN;
        }

        return;
    }

    if (button->state == STATE_MIDDLE_TO_DOWN)
    {
        if (value == button->gpio_value_down)
        {
            button->state = STATE_DOWN;
            (*button->callback)(button, EVENT_BUTTON_DOWN, button->data);
        }
        else
        {
            button->state = STATE_UP;
        }

        return;
    }
}

BUTTON * button_create(int gpio_num, enum GPIO_Value gpio_value_down, enum GPIO_Value gpio_value_up, BUTTON_CALLBACK callback, void * data)
{    
    GPIO * gpio = gpio_open(gpio_num, GPIO_IN, GPIO_BOTH);
    if (!gpio)
        return NULL;

    BUTTON * button = (BUTTON *) malloc(sizeof(BUTTON));
    button->gpio = gpio;
    button->timer_id = -1;
    button->state = STATE_UP;
    button->callback = callback;
    button->data = data;
    button->gpio_value_down = gpio_value_down;
    button->gpio_value_up = gpio_value_up;

    if (!loop_add_gpio_fd(gpio_get_fd(gpio), gpio_callback, button))
    {
        button_destroy(button);
        return NULL;
    }

    button->timer_id = loop_add_timer(timer_callback, button);

    if (button->timer_id == INVALID_TIMER_ID)
    {
        button_destroy(button);
        return NULL;
    }

    gpio_read(button->gpio);

    return button;
}

void button_destroy(BUTTON * button)
{
    if (button->timer_id != -1)
        loop_remove_timer(button->timer_id); // No method for destroying timer yet.

    if (button->gpio)
        gpio_close(button->gpio);

    free(button);
}
