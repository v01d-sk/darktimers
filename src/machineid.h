#ifndef MACHINEID_H_PLHWCNJE
#define MACHINEID_H_PLHWCNJE

/** Generate unique ID of the current machine as a string.
 *
 * Uses mac addresses at the moment.
 *
 * @param length Length of buffer. Must be at least 12.
 *
 * @return Negative number on failure or if length < 12.
 * @return Length of the ID in buffer on success.
 */
int machine_id(char* const buffer, const size_t length);

#endif /* end of include guard: MACHINEID_H_PLHWCNJE */
